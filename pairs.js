function pairs(object) {
    try {
        if (typeof object !== 'object' || object === null) {
            throw new TypeError("pairs function expects an object as an argument.");
        }

        const result = [];

        for (let key in object) {
            if (key in object) {
                result.push([key, object[key]]);
            }
        }

        return result;
    } catch (error) {
        console.error(`Error in pairs function: ${error.message}`);
        return []; 
    }
}

module.exports = pairs;
