function mapObject(object, callback) {
    try {
        if (typeof object !== 'object' || object === null) {
            throw new TypeError("Argument must be an object");
        }

        if (typeof callback !== 'function') {
            throw new TypeError("Callbacks must be a function");
        }


        const mappedObject = {};
        for (let key in object) {
            if (key in object) {
                mappedObject[key] = callback(object[key], key, object);
            }
        }

        return mappedObject;
    }
    catch (error) {
        console.error(error.message);
        return {};
    }
}

module.exports = mapObject;
