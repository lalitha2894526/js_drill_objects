function invert(object) {
    try {
        if (typeof object !== 'object' || object === null) {
            throw new TypeError("Argument must be an object");
        }

        const invertedObject = {};
        const keysArray = [];
        for (let propertyName in object) {
            keysArray.push(propertyName);
        }

        for (let index = 0; index < keysArray.length; index++) {
            const key = keysArray[index];
            const originalValue = object[key];
            invertedObject[originalValue] = key;
        }

        return invertedObject;
    } catch (error) {
        console.error("Error in invert function:", error.message);
        return {};
    }
}

module.exports = invert;
