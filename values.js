function values(object) {
    try {
        if (typeof object !== 'object' || object === null) {
            throw new TypeError("Argument must be an object");
        }

        const valuesArray = [];
        const keys = [];
        for (let key in object) {
            keys.push(key);
        }

        for (let index = 0; index < keys.length; index++) {
            const key = keys[index];
            valuesArray.push(object[key]);
        }

        return valuesArray;
    }
    catch (error) {
        console.error(error.message);
        return [];
    }
}

module.exports = values;
