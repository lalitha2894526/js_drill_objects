const invert = require('../invert');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("Original Object:");

console.log(testObject);

const invertedObject = invert(testObject);

console.log("Inverted Object:");
console.log(invertedObject);
