function keys(object) {
    try {
        if (typeof object !== 'object' || object === null) {
            throw new TypeError("Argument must be an object");
        }

        const keysArray = [];
        for (let propertyName in object) {
            keysArray.push(propertyName);
        }
        return keysArray;
    }
    catch (error) {
        console.error(error.message);
        return [];
    }
}
module.exports = keys;