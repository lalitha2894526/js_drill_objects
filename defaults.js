function defaults(object, defaultProperties) {
    try {
        if (typeof object !== 'object' || object === null || typeof defaultProperties !== 'object' || defaultProperties === null) {
            throw new TypeError("Arguments must be objects");
        }

        for (let key in defaultProperties) {
            if (object[key] === undefined) {
                object[key] = defaultProperties[key];
            }
        }

        return object;
    } catch (error) {
        console.error("Error in defaults function:", error.message);
        return {};
    }
}

module.exports = defaults;
