const mapObject = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function addKeyAndValue(value, key, object) {
    return `${key} -> ${value}`;
}

const mappedObject = mapObject(testObject, addKeyAndValue);
console.log(mappedObject);
