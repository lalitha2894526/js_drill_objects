const defaults = require('../defaults.js');

const testObject = { name: 'Bruce Wayne', location: 'Gotham' };
const defaultProperties = { name: 'Unknown', age: 30, alias: 'Batman' };

console.log("Original Object:");
console.log(testObject);

const updatedObject = defaults(testObject, defaultProperties);

console.log("Updated Object:");
console.log(updatedObject);
